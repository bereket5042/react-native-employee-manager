import React from 'react';
import {
	StyleSheet, View, Text
} from 'react-native';


const Header = (props) => {
	const {headerViewStyle, headerTextStyle} = styles;

	return (
		<View style={headerViewStyle}>
			<Text style={headerTextStyle}>{props.text}</Text>
		</View>
	);
}

const styles = {
	headerViewStyle:{
		flex: 0,
	    backgroundColor: '#f8f8f8',
	    alignItems: 'center',
	    justifyContent: 'flex-start',
	    height: 60,
	    paddingTop: 20,
	    shadowColor: '#000',
	    shadowOffset: { width: 0, height: 2},
	    shadowOpacity: 0.2,
	    elevation: 2,
	    position: 'relative',
	},

	headerTextStyle:{
		fontSize: 30,
	},
};

export { Header };