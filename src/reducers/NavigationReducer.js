import AppNavigator from '../AppNavigator';

const INITIAL_STATE = AppNavigator.router.getStateForAction(AppNavigator.router.getActionForPathAndParams(
	'LoginForm'));

const NavigationReducer = (state=INITIAL_STATE, action) => {
  const nextState = AppNavigator.router.getStateForAction(action, state);

  return nextState || state;
};

export default NavigationReducer;