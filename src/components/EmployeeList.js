import React from 'react';
import { View, Text, Button } from 'react-native';

// import { Button } from './common';


export default class EmployeeList extends React.Component{

	static navigationOptions = ({navigation}) => {
		return {
			title: "Employee List",
			headerRight: (
				<Button 
					title="Add"
					onPress={() => navigation.navigate('EmployeeCreate')}
				/>
			),
			headerStyle: {
		      backgroundColor: '#007aff',
		    },
		    headerTintColor: '#fff',
		    headerTitleStyle: {
		      fontWeight: 'bold',
		    },
		}
	};

	render(){
		return(
			<View>
				<Text>Employee</Text>
				<Text>Employee</Text>
				<Text>Employee</Text>
				<Text>Employee</Text>
				<Text>Employee</Text>
				<Text>Employee</Text>
			</View>
		);
	}
}