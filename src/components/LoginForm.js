import React from 'react';
import { connect } from 'react-redux';
import { Text } from 'react-native';
import { 
	Card, 
	CardSection, 
	Button, 
	CustomTextInput,
	Spinner
} from './common';
import { 
	emailChanged,
	passwordChanged,
	loginUser
} from '../actions';

class LoginForm extends React.Component{

	static navigationOptions = {
		title: "Please Login",
		headerStyle: {
	      backgroundColor: '#007aff',
	    },
	    headerTintColor: '#fff',
	    headerTitleStyle: {
	      fontWeight: 'bold',
	    },
	};

	onEmailChange(text){
		this.props.dispatchAction(emailChanged(text));
	}

	onPasswordChanged(text){
		this.props.dispatchAction(passwordChanged(text));
	}

	onButtonPress(){
		this.props.dispatchAction(loginUser({
			email: this.props.email,
			password: this.props.password
		}));
	}

	renderButton(){
		if(this.props.loading){
			return <Spinner />;
		}

		return(
			<Button onPress={this.onButtonPress.bind(this)} >
				Login
			</Button>
		);
	}

	render(){
		return(
			<Card>
				<CardSection>
					<CustomTextInput
						label="Email"
						placeholder="user@gmail.com"
						onChangeText={this.onEmailChange.bind(this)}
						value={this.props.email}
					/>
				</CardSection>
				<CardSection>
					<CustomTextInput
						secureTextEntry
						label="Password"
						placeholder="password"
						onChangeText={this.onPasswordChanged.bind(this)}
						value={this.props.password}
					/>
				</CardSection>

				<Text style={styles.errorTextStyle} >
					{this.props.error}
				</Text>

				<CardSection>
					{ this.renderButton() }
				</CardSection>
			</Card>
		);
	}
}

const styles = {
	errorTextStyle: {
		fontSize: 20,
		alignSelf: 'center',
		color: 'red'
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		dispatchAction: (action) => dispatch(action)
	}
	
};

const mapStateToProps = (state) => {
	return {
		email: state.auth.email,
		password: state.auth.password,
		error: state.auth.error,
		loading: state.auth.loading
	}
};


export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);