import { combineReducers } from 'redux';

import AuthReducer from './AuthReducer';
import NavigationReducer from './NavigationReducer';
import EmployeeFormReducer from './EmployeeFormReducer'

const rootReducer = combineReducers({
	auth: AuthReducer,
	nav: NavigationReducer,
	employee: EmployeeFormReducer
});


export default rootReducer;