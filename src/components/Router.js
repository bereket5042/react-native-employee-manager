import { StackNavigator } from 'react-navigation';
import LoginForm from './LoginForm';

export default StackNavigator({
	LoginForm: {
		screen: LoginForm,
	},
});