import { StackNavigator, SwitchNavigator } from 'react-navigation';
import LoginForm from './components/LoginForm';
import EmployeeList from './components/EmployeeList';
import EmployeeCreate from './components/EmployeeCreate';


export default AppNavigator = StackNavigator({
	LoginForm: {
		screen: LoginForm,
	},
	EmployeeList: {
		screen: EmployeeList,
	},
	EmployeeCreate: {
		screen: EmployeeCreate,
	}
},
{
	initialRouteName: 'LoginForm'
}
);
// const Auth = StackNavigator({
// 	LoginForm: {
// 		screen: LoginForm,
// 	}
// });

// const Main = StackNavigator({
// 	EmployeeList: {
// 		screen: EmployeeList
// 	}
// });

// export default AppNavigator = SwitchNavigator(
// 	{
// 		Auth: Auth,
// 		Main: Main
// 	},
// 	{
// 		initialRouteName: 'Auth'
// 	}
// );