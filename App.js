import React from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';

import rootReducer from './src/reducers';
import LoginForm from './src/components/LoginForm';
import { Header } from './src/components/common';
import AppNavigation from './src/AppNavigation';
import { middleware } from './src/utils/redux-helpers';

export default class App extends React.Component {

  componentWillMount(){
    const config = {
      apiKey: "AIzaSyAWxeOejw7CehyXEja_ZC_d88Dos8DFjBc",
      authDomain: "manager-f45dc.firebaseapp.com",
      databaseURL: "https://manager-f45dc.firebaseio.com",
      projectId: "manager-f45dc",
      storageBucket: "",
      messagingSenderId: "116792518557"
    };

    firebase.initializeApp(config);
  }

  render() {
    const store = createStore(
      rootReducer,
      {},
      applyMiddleware(middleware, ReduxThunk)
    );
    return (
      <Provider store = {store} >
        <AppNavigation />
      </Provider>
    );
  }
}