import React from 'react';
import { connect } from 'react-redux';
import { Card, CardSection, CustomTextInput, Button } from './common';

import { employeeUpdate } from '../actions/EmployeeActions';


class EmployeeCreate extends React.Component{

	static navigationOptions = {
		title: "Employee Create",
		headerStyle: {
	      backgroundColor: '#007aff',
	    },
	    headerTintColor: '#fff',
	    headerTitleStyle: {
	      fontWeight: 'bold',
	    },
	};

	onChangeInput(prop, value){
		this.props.employeeUpdate({prop, value});
	}

	render(){

		return(
			<Card>
				<CardSection>
					<CustomTextInput
						label="Name"
						placeholder="name"
						onChangeText={(name) => {this.onChangeInput("name", name)}}
						value={this.props.name}
					/>
				</CardSection>
				<CardSection>
					<CustomTextInput
						label="Phone"
						placeholder="+2519323233"
						onChangeText={(phone) => {this.onChangeInput("phone", phone)}}
						value={this.props.phone}
					/>
				</CardSection>
				<CardSection>

				</CardSection>
				<CardSection>
					<Button>
						Create
					</Button>
				</CardSection>
			</Card>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		name: state.employee.name,
		phone: state.employee.phone
	};
};

export default connect(mapStateToProps, { employeeUpdate })(EmployeeCreate);